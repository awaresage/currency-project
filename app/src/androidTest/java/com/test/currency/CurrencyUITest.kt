package com.test.currency

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.test.currency.ui.CurrencyActivity
import com.test.currency.util.EspressoIdlingResource
import org.hamcrest.core.IsNot.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CurrencyUITest {

    @get:Rule
    var activityRule = ActivityTestRule(CurrencyActivity::class.java)

    @Before
    fun init() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun shouldChangeCurrencyText() {
        EspressoIdlingResource.increment()
        Thread.sleep(1000)
        onView(ViewMatchers.withId(R.id.plnValueView)).check(matches(withText(not("0.0"))))
    }
}