package com.test.currency.app

import android.app.Application
import com.test.currency.network.networkModule
import com.test.currency.ui.currencyModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyApp : Application() {

    companion object {
        private lateinit var app: CurrencyApp
        fun app(): CurrencyApp {
            return app
        }
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        startKoin {
            androidContext(this@CurrencyApp)
            modules(listOf(networkModule, currencyModule))
        }
    }
}