package com.test.currency.network

import com.test.currency.model.Currency
import retrofit2.http.GET

interface CurrencyApi {

    @GET("latest")
    suspend fun getCurrency(): Currency
}