package com.test.currency.ui

import android.os.Handler
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.currency.model.Currency
import com.test.currency.repository.CurrencyRepository
import com.test.currency.util.EspressoIdlingResource
import kotlinx.coroutines.launch
import retrofit2.HttpException

class CurrencyViewModel(private val currencyRepository: CurrencyRepository) : ViewModel() {

    companion object {
        val TAG: String = CurrencyViewModel::class.java.simpleName

        private const val DELAY = 5 * 1000L
    }

    var currencyLiveData = MutableLiveData<Currency>()

    var currency: Currency? = null

    private val handler = Handler()

    private val runnable = Runnable {
        updateCurrency()
    }

    private fun startPing() {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, DELAY)
    }

    fun updateCurrency() {
        viewModelScope.launch {
            try {
                currency = currencyRepository.updateCurrency()
                currencyLiveData.value = currency
                EspressoIdlingResource.decrement()
                Log.d(TAG, "PLN: ${currency!!.rates.PLN} USD: ${currency!!.rates.USD}")
            } catch (e: HttpException) {
                Log.e(TAG, "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e(TAG, "Something else went wrong")
            } finally {
                startPing()
            }
        }
    }

    override fun onCleared() {
        handler.removeCallbacks(runnable)
        super.onCleared()
    }
}