package com.test.currency.ui

import com.test.currency.network.CurrencyApi
import com.test.currency.repository.CurrencyRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val currencyModule = module {

    single { provideCurrencyRepository(get()) }

    viewModel { provideCurrencyViewModule(get()) }
}

fun provideCurrencyRepository(currencyApi: CurrencyApi) = CurrencyRepository(currencyApi)

fun provideCurrencyViewModule(currencyRepository: CurrencyRepository) = CurrencyViewModel(currencyRepository)