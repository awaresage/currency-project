package com.test.currency.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.test.currency.R
import com.test.currency.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class CurrencyActivity : AppCompatActivity() {

    private val currencyViewModel: CurrencyViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.currencyViewModel = currencyViewModel
        binding.lifecycleOwner = this
    }

    override fun onResume() {
        super.onResume()
        currencyViewModel.updateCurrency()
    }
}
