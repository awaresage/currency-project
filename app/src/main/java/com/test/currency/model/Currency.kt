package com.test.currency.model

data class Currency(val rates: Rate)