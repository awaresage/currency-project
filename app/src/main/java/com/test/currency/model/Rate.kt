package com.test.currency.model

data class Rate(var USD: Double, var PLN: Double)