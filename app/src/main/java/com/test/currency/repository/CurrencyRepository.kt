package com.test.currency.repository

import com.test.currency.model.Currency
import com.test.currency.network.CurrencyApi

class CurrencyRepository(private val currencyApi: CurrencyApi) {

    suspend fun updateCurrency(): Currency {
        val currency = currencyApi.getCurrency()
        return currency
    }
}